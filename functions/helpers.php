<?php
/**
 * Gateway helper functions and definitions
 *
 */

function gabriele_blum_build_svg( $svg_src, $fallback = false) {
	if ( ! is_string( $svg_src ) || '' === $svg_src ) {
		return;
	}
	if ( ! is_readable( $svg_src ) ) {
		return;
	}
	$svg = file_get_contents( $svg_src );
	if ( $fallback ) {
		$png_src = str_replace( 'svg', 'png', $svg_src );	$svg = gabriele_blum_insert_svg_fallback( $svg, $png_src );
	}
	return $svg;
}

function gabriele_blum_insert_svg_fallback( $svg, $png_src ) {
	// Add png fallback if available.
	if ( ! is_readable( $png_src ) ) {
		return $svg;
	} else {
		$png_insert = '<image src="' . $png_src . '" xlink:href=""></svg>';
		$svg = substr_replace( $svg, $png_insert, -6 );
		return $svg;
	}
}

/**
 * Retrieves the attachment ID from the file 
 *
 * @return void
 * @author Pippins Plugins
 * @link https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
 **/
function gabriele_blum_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}

function gabriele_blum_the_child_page_buttons( $string = '' ) {
    $args = array(
			'post_parent' => get_the_ID(),
			'post_type'   => 'page',
			'numberposts' => 5,
			'post_status' => 'any',
			'orderby'     => 'menu_order',
			'order'       => 'asc',
		);
	$child_pages = get_posts( $args );
	if ( empty( $child_pages ) ) {
		return;
	}
	foreach ( $child_pages as $child_page ) : ?>
		<?php if ( ! empty( $string ) ) {
			$button_text = wp_sprintf( $string, strtolower( $child_page->post_title ) );
		} else {
			$button_text = $child_page->post_title;
		}
		?>
		<a class="button" href="<?php echo esc_url( get_permalink( $child_page->ID ) ); ?>">
			<?php echo esc_html( $button_text ); ?>
		</a>
	<?php endforeach;

}

// Numeric Page Navi (built into the theme by default)
function gabriele_blum_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ul class="pagination">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( 'First', 'gabriele-blum' );
		echo '<li><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li>';
	previous_posts_link( __('Previous', 'gabriele-blum') );
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="current"> '.$i.' </li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li>';
	next_posts_link( __('Next', 'gabriele-blum') );
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( 'Last', 'gabriele-blum' );
		echo '<li><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ul></nav>'.$after."";
} /* End page navi */

function gabriele_blum_the_grid_column_class($return = false) {
	if ( ! is_single() ) {
		$output = esc_attr( 'xxlarge-2 large-3 medium-4 small-6 columns' );
	} else {
		$output = esc_attr( 'xxlarge-3 large-4 medium-6 small-6 columns' );
	}
    if ( $return ) {
        return $output;
    } else {
        echo $output;
    }
}

function gabriele_blum_get_project_meta( $key = '' ) {
    if ( empty( $key ) ) {
        return;
    }
    $prefix = 'gabriele_blum_project_';
	$value = get_post_meta( get_the_ID(), $prefix . $key, true );
	if ( empty( $value ) ) {
		return false;
	} else {
		return $value;
	}
}

function gabriele_blum_the_publisher() {
	$terms = get_the_term_list( get_the_ID(), 'publisher', '', ' | ' );
	if ( ! empty( $terms ) && ! $terms instanceof WP_Error ) {
		echo $terms;
	}
}

function gabriele_blum_the_title() {
    $author = gabriele_blum_get_project_meta('author');
    $series = gabriele_blum_get_the_series();
    $series_class = empty( $series ) ? '' : ' project__title--has-series';
    $title = '<h2 class="title project__title' . $series_class . '">' . get_the_title() . '</h2>';
    $title .= $series;
    if ( ! empty( $author ) ) {
        $title = '<div class="project__author"><span itemprop="author">' . $author . '</span></div>' . $title;
    }
    echo $title;
};

function gabriele_blum_get_the_series() {
    $series = get_the_terms( get_the_ID(), 'series' );
    if ( false === $series || $series instanceof WP_Error ) {
        return;
    }
    $series_string = '<span itemprop="isPartOf">' . $series[0]->name . '</span>';
    $number = gabriele_blum_get_project_meta( 'series_number' );
    if ( is_numeric( $number ) ) {
        $series_string .= ' <span itemprop="position">' . $number . '</span>';
    }
    return '<div class="project__series">' . $series_string . '</div>';
}

add_action('gabriele_blum_change_titles',function() {
    if ( ! is_user_logged_in() ) {
        return;
    }
    wp_reset_query();
    $arguments = [
            'post_type' => 'project',
            'status' => 'publish',
            'posts_per_page' => -1,
    ];
    $change_query = new WP_Query( $arguments );
    if ( $change_query->have_posts()) {
        foreach( $change_query->posts as $project ) {
            $title = $project->post_title;
            $colon = strpos( $title, ':');
            $updated = false;
            if (( $colon ) > -1 ) {
                $author = substr($title, 0, $colon);
                if ( ! empty( $author ) ) {
                    $updated = add_post_meta($project->ID, 'gabriele_blum_project_author', $author, true);
                }
            }
            if ( $updated ) {
                $new_title = str_replace($author . ':', '', $title );
                $count = 1;
                if ( strpos( $new_title,'<br />') === 0 ) {
                    $new_title = str_replace('<br />', '',$new_title, $count );
                }
                $updated_project = wp_update_post([
                    'ID' => $project->ID,
                    'post_title' => $new_title,
                    ]
                );
                if ( is_wp_error( $updated_project ) ) {
                    $errors = $updated_project->get_error_messages();
                    foreach( $errors as $error ) {
                        echo $error;
                    }
                } else {
                    echo "Updated project with ID " . $updated_project . " with author: " . get_post_meta( $project->ID, 'gabriele_blum_project_author', true ) . "<br />";
                }
            }
        }
    }
});
