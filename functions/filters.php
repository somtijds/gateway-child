<?php
/**
 * Gateway Child script filters and actions
 *
 */
add_action('pre_get_posts','action_gabriele_blum_pre_get_posts');

function action_gabriele_blum_pre_get_posts( $query ) {
	if ( $query->is_category() ) {
		$query->set('post_type','project');
	}
}

add_filter( 'manage_taxonomies_for_project_columns', 'action_gabriele_blum_project_type_columns' );

function action_gabriele_blum_project_type_columns( $taxonomies ) {
    $taxonomies[] = 'feature';
    return $taxonomies;
}

add_filter( 'the_title', 'action_gabriele_blum_break_line_after_colon');

function action_gabriele_blum_break_line_after_colon( $title ) {
    $count = 1;
    return str_replace(':', ':<br />', $title, $count);
}