<?php 
/**
 * Gateway script enqueueing functions and definitions
 *
 */

function gabriele_enqueue_scripts_and_styles() {
	/**
 	* Get the theme's version number for cache busting
	*/
	$gateway = wp_get_theme();
	wp_enqueue_style( 'gateway-parent-styles', get_template_directory_uri() . '/style.css',  array(), $gateway['Version'] );
	wp_enqueue_style( 'gateway-child-styles', get_stylesheet_directory_uri() . '/assets/dist/css/gateway-child.css', array(), '' );

    // Slick slider JS
    wp_enqueue_script(
        'slick-js',
        get_stylesheet_directory_uri() . '/assets/lib/slick-carousel/slick/slick.min.js',
        array( 'jquery' ),
        null
    );
    // Slick slider CSS
    wp_enqueue_style(
        'slick-css',
        get_stylesheet_directory_uri() . '/assets/lib/slick-carousel/slick/slick.css',
        null
    );
    // Slick slider CSS
    wp_enqueue_style(
        'slick-theme-css',
        get_stylesheet_directory_uri() . '/assets/lib/slick-carousel/slick/slick-theme.css',
        null
    );
    if ( is_front_page() ) {
        wp_enqueue_script(
            'slick-init-js',
            get_stylesheet_directory_uri() . '/assets/dist/js/slider.init.js',
            array('slick-js'),
            null
        );
    }
}
add_action( 'wp_enqueue_scripts','gabriele_enqueue_scripts_and_styles' );