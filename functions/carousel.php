<?php
/**
 * Gateway Child carousel shortcode
 *
 */
add_shortcode('carousel','gabriele_blum_carousel');
function gabriele_blum_carousel( $atts = [] ) {

    $atts = shortcode_atts(
            [
                    'header' => __('Neues und interessantes','gabriele'),
                    'show' => '5'
            ],
            array_change_key_case((array)$atts, CASE_LOWER)
    );

    if (
        !post_type_exists('project') ||
        !taxonomy_exists('feature')
    ) {
        return;
    }
    // Get chosen category
    $home_projects_cat = esc_attr( get_theme_mod( 'home_posts_cat' ) );
    $home_projects_term = 'carousel-home';

    // WP_Query arguments
    $args = array (
        'post_type'         => 'project',
        'cat'         	    => $home_projects_cat,
        'posts_per_page'    => is_int($atts['show']) ? $atts['show'] : 10,
        'max_num_pages'     => '1',
        'tax_query'			=> array(
            array(
                'taxonomy'	=> 'feature',
                'field'		=> 'slug',
                'terms'     => $home_projects_term,
            )
        )
    );

    $projects_query = new WP_Query( $args );

    $output = '';

    ob_start();

    if ( $projects_query->have_posts() ) : ?>
    <section class="carousel">

        <div class="project-slider carousel__slider">

        <?php if ( is_string( $atts['header'] ) && ! empty( $atts['header'] ) ) : ?>

        <?php endif; ?>
        <?php // Loop over projects.

            while ( $projects_query->have_posts() ) {

                $projects_query->the_post();

                get_template_part('parts/loop-home-slider');

            } ?>

        </div><!--project-slider-->
    </section>
    <?php endif;

    // Restore original post data
    wp_reset_postdata();

    $output = ob_get_clean();

    return $output;
}