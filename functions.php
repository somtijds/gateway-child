<?php
/**
 * Gateway Child script functions and definitions
 *
 */
require_once get_stylesheet_directory() . '/functions/enqueue_scripts.php';
require_once get_stylesheet_directory() . '/functions/helpers.php';
require_once get_stylesheet_directory() . '/functions/filters.php';
require_once get_stylesheet_directory() . '/functions/carousel.php';