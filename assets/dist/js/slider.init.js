jQuery(document).ready(function() {
    jQuery(".project-slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true
    });
});