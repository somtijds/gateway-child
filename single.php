<?php
/**
 * The template for displaying all single posts.
 *
 */

get_header(); ?>

<div class="row">

	<div id="primary" class="content-area">

		<div class="large-8 columns">

			<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'parts/content', 'single-' . get_post_type() ); ?>

				<hr>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- .large-8 -->

        <div class="large-3 large-offset-1 columns">
            <?php if ( is_singular('project')) : ?>
                <h3 class="widget-title">Schlagwörter</h3>
                <div class="widget_tag_cloud widget_tag_cloud--sidebar">
                    <?php echo get_the_tag_list(); ?>
                </div>
            <?php else: ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>
        </div><!-- .large-3 -->

	</div><!-- #primary -->



</div><!-- .row -->

<?php get_footer(); ?>