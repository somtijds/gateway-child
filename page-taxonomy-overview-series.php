<?php
/**
 * Template name: Taxonomie-Übersicht (Series)
 */

get_header(); ?>

<div class="row">

	<div id="primary" class="content-area">

		<div class="large-8 large-push-2 columns">

			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php
                        $slug = 'series';
                        if ( taxonomy_exists( $slug ) ) : ?>
                        <ul class="term-list">
                            <?php $terms = get_terms( $slug, ['hide_empty' => true]);
                                if ( ! empty( $terms ) ) :
                                foreach ( $terms as $term ) : ?>
                                    <li><a href="<?php echo esc_url( get_term_link( $term ) ); ?>">
                                            <?php esc_html_e( $term->name ); ?>
                                        </a>
                                        <?php if ( ! empty( $term->description ) ) : ?>
                                        <p><?php esc_html_e( $term->description ); ?></p>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    <?php endif; ?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- .large-8 -->
		
	</div><!-- #primary -->

</div><!-- .row -->

<?php get_footer(); ?>