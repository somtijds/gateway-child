<?php get_header(); ?>
	
	<?php 
		global $wp_query;
		$paged_class = $wp_query->max_num_pages > 1 ? ' paged' : ''; ?>

	<main id="content" class="grid<?php echo esc_attr( $paged_class ); ?>" role="main">

	<?php if (have_posts()) : ?>

        <?php the_archive_title('<h2 class="archive__title">','</h2>'); ?>
		
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>

		<?php endwhile; ?>	

	<?php else : ?>
								
		<?php get_template_part( 'parts/content-none', 'missing' ); ?>
			
	<?php endif; ?>

</main> <!-- end #main -->

<?php if ( have_posts() ) : ?>
	<?php gabriele_blum_page_navi(); ?>
<?php endif; ?>


<?php get_footer(); ?>