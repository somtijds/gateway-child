<div class="site-branding">

<?php $header_logo = get_theme_mod( 'header_logo' ); if ( ! $header_logo ) { ?>

  <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" alt="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

  <h2><?php bloginfo( 'description' ); ?></h2>

<?php } else { ?>

  <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?php echo esc_url( $header_logo ); ?>" alt=""></a>

<?php } ?>

</div><!-- .site-branding -->