<?php
/**
 * The template used for displaying page content in single.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype ="http://schema.org/Audiobook">

	<div class="featured-image">
		<?php // Featured Image
		if ( has_post_thumbnail() ) { 

			echo '<a href="' . esc_url( get_permalink() ) . '">';
			the_post_thumbnail( 'gateway-post-image' );
			echo '</a>';

		} // end featured image ?>
	</div><!-- .featured-image -->

	<header class="entry-header">
        <?php gabriele_blum_the_title(); ?>

        <?php get_template_part('parts/content','project-meta'); ?>


	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'gateway' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer clearfix">


	</footer><!-- .entry-footer -->

</article><!-- #post-## -->