<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<section class="no-results not-found">

    <div class="row">

        <div class="mall-12 medium-8 medium-offset-2 large-6 large-offset-3 columns">

            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Ups! Leider haben wir hier nichts gefunden.', 'gabriele-blum' ); ?></h1>
            </header><!-- .page-header -->

        </div> <!-- .large-12 -->

    </div> <!-- .row -->
    <div class="row">

        <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3 columns">

            <p><?php get_search_form(); ?></p>

            <p><?php _e( 'Vielleicht kannst du hier was suchen?', 'gabriele-blum' ); ?></p>

        </div><!-- .large-12 -->

    </div><!-- .row -->

</section><!-- .no-results -->
