<!--Item: -->

<article id="post-<?php the_ID(); ?>" <?php post_class(gabriele_blum_the_grid_column_class(true)) ?> role="article" data-equalizer-watch >
    <?php if ( has_post_thumbnail() ) : ?>
        <section class="featured-image" itemprop="articleBody">

            <a href="<?php echo get_the_permalink(); ?>" rel="bookmark" title="<?php echo esc_attr( 'Navigiere zu der Seite vom Verlag','gabriele-blum' ); ?>">

            <?php the_post_thumbnail('medium'); ?>

            </a>

        </section> <!-- end article section -->

    <?php else : ?>

            <section class="featured-image placeholder" itemprop="articleBody">
                <a href="<?php echo get_the_permalink(); ?>" rel="bookmark" title="<?php echo esc_attr( 'Navigiere zu der Seite vom Verlag','gabriele-blum' ); ?>">
                <svg class="wp-post-image" viewBox="0 0 100 60">
                  <g>
                    <rect x="25%" y=0 height="100%" width="50%" stroke-width="0.667" stroke="#D63240" fill="none" />
                    <text x="50%" y="40%" text-anchor="middle"><?php _e( 'Bild','gabriele-blum' ); ?></text>
                    <text x="50%" y="55%" text-anchor="middle"><?php _e( 'kommt','gabriele-blum' ); ?></text>
                    <text x="50%" y="70%" text-anchor="middle"><?php _e( 'bald!','gabriele-blum' ); ?></text>
                  </g>
                </svg>
            </section> <!-- end article section -->

    <?php endif; ?>

    <header class="article-header">
        <?php gabriele_blum_the_title(); ?></h2>
        <?php gabriele_blum_the_publisher(); ?>
    </header> <!-- end article header -->

</article> <!-- end article -->
