<?php
/**
 * Page loop template.
 *
 * @package elami
 **/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

		<div class="section-wrapper">

			<section class="entry-content" itemprop="articleBody">

				<?php if ( has_post_thumbnail() ) : ?>
					<aside class="entry-image section__image">
						<?php echo get_the_post_thumbnail( $post->ID, 'medium_large' ); ?>
					</aside>
				<?php endif; ?>

				<div class="section__text">
					<header class="article-header section__header">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php
							$subtitle = get_post_meta( $post->ID, 'gabriele_blum_page_subtitle', true );
							if ( ! empty( $subtitle ) ) : ?>
							<span class="section__header__subtitle subheader">
								<?php echo esc_html( $subtitle ); ?>
							</span>
						<?php endif; ?>
					</header> <!-- end article header -->

					    <?php the_content(); ?>

				</div>

			    <?php /* wp_link_pages(); */ ?>

			</section> <!-- end article section -->

		</div><!-- section-wrapper-->

</article> <!-- end article -->
