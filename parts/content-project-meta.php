<?php
/**
 * Date: 08.12.18
 * Time: 17:12
 */
?>

<div class="entry-meta">

    <?php echo get_the_term_list(get_the_ID(),'genre','<div class="project__meta project__genre">',', ','</div>'); ?>

    <?php
        $duration = gabriele_blum_get_project_meta('duration');
        if (! empty( $duration ) ) : ?>
        <div class=" project__meta project__duration" itemprop="duration"><?php echo esc_html( $duration ); ?></div>
    <?php endif; ?>

    <?php
    $actors = gabriele_blum_get_project_meta('actors');
    if (! empty( $actors ) ) : ?>
        <div class="project__meta project__actors" itemprop="readBy"><?php echo esc_html( $actors ); ?></div>
    <?php endif; ?>
    
    <?php
        $terms = get_the_terms(get_the_ID(),'publisher');
        $publisher_link = gabriele_blum_get_project_meta('url');
        //$audible = strpos( $publisher_link, 'audible' ) ? ' | Audible' : '';
        //$audible = strpos( $publisher_link, 'amazon' ) ? ' | Amazon' : $audible;
        if (is_array( $terms ) && $terms[0] instanceof WP_Term ) : ?>
        <div class="project__meta project__publisher-link">
    <?php if ( ! empty( $publisher_link ) ) : ?>
       <?php $publisher_link_string = '<a target="_blank" href="' . esc_url( $publisher_link ) . '"><span itemprop="publisher">' . $terms[0]->name . '</span></a>';
    echo sprintf(__('Reinhören bei %s','gabriele blum' ), $publisher_link_string ); ?>
    <?php else: ?>
    <a target="_blank" href="<?php echo esc_url( get_term_link( $terms[0] ) ); ?>"><span itemprop="publisher"><?php echo esc_html( $terms[0]->name ); ?></span></a>
    <?php endif; ?>
    </div>
    <?php endif; ?>
    
</div><!-- .entry-meta -->