<header id="masthead" class="site-header" role="banner">
	<div class="stick">
	
		<?php get_template_part( 'parts/header-site-branding.php'); ?>

		<nav class="top-bar" data-topbar data-options="mobile_show_parent_link: true">

		<ul class="title-area">
			<li class="name"></li>
			<li class="toggle-topbar menu-icon"><a href="#"><span><?php _e('Menu','gateway'); ?></span></a></li>
		</ul>

		<section class="top-bar-section">
				<?php 
				$defaults = array(
						'theme_location' =>  'primary',
						'container'      =>  false,
						'menu_class'     =>  'right',
						'depth'          =>  5,
						'fallback_cb'    =>  'gateway_demo_header_nav', // located at 'inc/template-tags.php'
						'items_wrap'     =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'walker'         =>  new gateway_foundation_walker()
				);

				wp_nav_menu( $defaults );
				?>
		</section>
		</nav>

		<hr>

	</div><!-- .stick -->
</header><!-- .row #masthead -->
