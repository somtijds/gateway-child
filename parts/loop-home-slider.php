<div <?php post_class('slide'); ?> data-toggler=".active">
    <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail('medium',['class'=>'slide__image']); ?>
    </a>
    <header>
        <h2 class="slide__title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h2>
    </header>
</div>

