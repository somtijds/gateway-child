<?php
/**
 *
 * Template Name: Home
 *
 */

get_header( 'home' ); ?>



<div class="row home-content">
	
	<div class="large-12 columns">

        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'template-parts/content', 'page' ); ?>

        <?php endwhile; // end of the loop. ?>

	</div><!-- .large-12 -->

</div><!-- .home-content -->

<?php get_footer(); ?>
