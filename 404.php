<?php
/**
 * The template for displaying 404 pages (not found).
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">

				<div class="row">

					<div class="large-12 columns">

						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Ups! Leider haben wir hier nichts gefunden.', 'gabriele-blum' ); ?></h1>
						</header><!-- .page-header -->

					</div> <!-- .large-12 -->

				</div> <!-- .row -->

				<div class="page-content">

				<div class="row">

					<div class="large-12 columns">

                        <?php get_search_form(); ?>

                        <p><?php _e( 'Vielleicht kannst du hier was suchen?', 'gabriele-blum' ); ?></p>

					</div><!-- .large-12 -->

				</div><!-- .row -->

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>
